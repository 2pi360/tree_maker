from statistics import mean
import simplified_pygame as spg
import tree_base
from presets import PRESETS


class AppControlls(spg.EventReaderAsClass):

    def on_key_escape():
        SCREEN.exit()



def procrustes(a, x, b):
    return max(a, min(x, b))



DARK = (100, 100, 100)
LIGHT = (200, 200, 200)


class SingleController(spg.EventReader):

    def __init__(self, screen_size, y, init=0.5, text=''):
        self.screen_size = screen_size
        self.y = y
        self.val = init
        self.next_val = init
        self.text = text
        self.dv = 0
        tree_base.PARAMETERS[text] = self

    def mouse_map(self, x, y):
        sw, sh = self.screen_size
        if spg.in_box(x, y, (sw-245, self.y-20, 210, 30)):
            return procrustes(0, (x - sw+240) / 200, 1)

    def draw(self, screen):
        sw, sh = self.screen_size
        with screen.part(sw-245, self.y-10, 210, 21) as part:
            xv = int(200*self.val)+5
            xn = int(200*self.next_val)+5
            if self._mouse_pos:
                part.fill((255, 200, 200, 100))
            part.line([105, 5, 0, 11], DARK)
            part.box([5, 9, 200, 3], DARK)
            part.box([xn-5, 0, 11, 21], LIGHT)
            part.rect([xv-5, 0, 11, 21], LIGHT)

        screen.write(sw-240, self.y-24, self.text, col=LIGHT)

    def on_mouse_click(self, v):
        if 0.45 < v < 0.55:
            self.set_vals(0.5)
        else:
            self.set_vals(v)

    def on_mouse_right_click(self, v):
        self.set_vals(v)

    def set_vals(self, v):
        self.next_val = v
        self.dv = (self.next_val - self.val) / 1000

    def move_vals(self, dt):
        if self.val > self.next_val:
            self.val = max(self.next_val, self.val+self.dv * dt)
        elif self.val < self.next_val:
            self.val = min(self.next_val, self.val+self.dv * dt)

    def to_json(self):
        return round(self.val, 2)

    def __call__(self):
        return self.val


class MultipleController(spg.EventReader):

    def __init__(self, screen_size, y, init=0.5, text=''):
        self.screen_size = screen_size
        self.y = y
        if isinstance(init, list):
            self.val = init[:]
            self.next_val = init[:]
        else:
            self.val = [init] * 10
            self.next_val = [init] * 10
        self.dv = [0] * 10
        self.text = text
        tree_base.PARAMETERS[text] = self

    def mouse_map(self, x, y):
        sw, sh = self.screen_size
        if spg.in_box(x, y, (sw-245, self.y-10, 210, 219)):
            return 10 - int((y - self.y+10) / 20), procrustes(0, (x - sw+240) / 200, 1),

    def draw(self, screen):
        sw, sh = self.screen_size
        with screen.part(sw-245, self.y-10, 210, 221) as part:
            xv = [int(200*v)+5 for v in self.val]
            xn = [int(200*v)+5 for v in self.next_val]
            if self._mouse_pos:
                part.fill((255, 200, 200, 100))
                i = self._mouse_pos[0]
                part.rect([5, (10-i)*20, 220, 20], (255, 200, 200, 200))

            for i in range(10):
                y = 25 + i*20
                part.line([105, y, 0, 11], DARK)
                part.box([5, y+4, 200, 3], DARK)
                part.box([xn[i]-5, y, 11, 11], DARK)
                part.rect([xv[i]-5, y, 11, 11], DARK)

            part.lines(xv, [30 + i*20 for i in range(10)], DARK)

            y = 5
            part.box([5, y+4, 200, 3], LIGHT)
            part.rect([mean(xv)-5, y, 11, 11], LIGHT)

        screen.write(sw-240, self.y-24, self.text, col=LIGHT)

    def on_mouse_click(self, pos):
        i, v = pos
        if 0.48 < v < 0.52:
            v = 0.5
        if i < 10:
            i = 9-i
            self.next_val[i] = v
            self.dv[i] = (self.next_val[i] - self.val[i]) / 1000
            if i > 1:
                self.next_val[i-1] = (self.next_val[i-2] + v) / 2
            if i < 8:
                self.next_val[i+1] = (self.next_val[i+2] + v) / 2
        else:
            dv = mean(self.next_val) - v
            self.next_val = [min(max(0, v-dv), 1) for v in self.next_val]
        self.set_vals(self.next_val)

    def on_mouse_rightclick(self, pos):
        i, v = pos
        if 0.48 < v < 0.52:
            v = 0.5
        if i < 10:
            i = 9-i
            self.next_val[i] = v
        else:
            self.next_val = [v]*10
        self.set_vals(self.next_val)

    def set_vals(self, v):
        if isinstance(v, list):
            self.next_val = v
        else:
            self.next_val = [v]*10
        self.dv = [(self.next_val[i] - self.val[i]) / 1000 for i in range(10)]

    def move_vals(self, dt):
        for i in range(10):
            if self.val[i] > self.next_val[i]:
                self.val[i] = max(self.next_val[i], self.val[i]+self.dv[i]*dt)
            elif self.val[i] < self.next_val[i]:
                self.val[i] = min(self.next_val[i], self.val[i]+self.dv[i]*dt)

    def to_json(self):
        if all(x == self.val[0] for x in self.val):
            return self.val[0]
        return [round(x, 2) for x in self.val]

    def __call__(self, x):
        i = int(x*9)
        if i == 9:
            return self.val[9]
        a, b = self.val[i], self.val[i+1]
        x = x*9 - i
        return b*x + a*(1-x)


class BranchingController(spg.EventReader):

    def __init__(self, screen_size, y, init_br=0, init_bal=0, text='Branching'):
        self.screen_size = screen_size
        self.y = y
        if isinstance(init_br, list):
            self.branches = init_br[:]
        else:
            self.branches = [init_br] * 10

        if isinstance(init_bal, list):
            self.balance = init_bal[:]
        else:
            self.balance = [init_bal] * 10

        self.next_balance = self.balance
        self.dv = [0] * 10
        self.text = text
        tree_base.PARAMETERS[text] = self

    def mouse_map(self, x, y):
        sw, sh = self.screen_size
        if spg.in_box(x, y, (sw-240, self.y-10, 200, 239)):
            i = int((y - self.y+10) / 22)
            j = int((x - sw+240) / 20)
            if j <= i:
                return 9-i, j
            return None

    def draw(self, screen):
        sw, sh = self.screen_size
        with screen.part(sw-240, self.y-10, 210, 243, (130, 160, 130)) as part:
            if self._mouse_pos:
                si, sj = self._mouse_pos
            else:
                si, sj = None, None

            for i in range(10):
                col = (255, 200, 200, 150 if si==i else 70 if self.branches[i] else 25)
                for j in range(10-i):
                    part.rect([20*j, (9-i)*22, 19, 20], col)
                if si==i:
                    part.rect([20*sj, (9-i)*22, 19, 20], (255, 200, 200, 200))
                if self.branches[i]:
                    part.write(self.next_balance[i]*20+5, (9-i)*22+5, str(self.branches[i]))
                    part.write(self.balance[i]*20+5, (9-i)*22+5, str(self.branches[i]), bold=True, col=0)

            if si == -1:
                part.rect([20*sj, 220, 19, 20], (255, 200, 200, 200))
            for i in range(10):
                part.write(i*20+5, 225, '^', bold=True)

        screen.write(sw-240, self.y-24, self.text, col=LIGHT)

    def on_mouse_click(self, pos):
        level, balance = pos
        new_bal = self.next_balance[:]
        if level == -1:
            new_bal = [balance / 9 * (9-i) for i in range(10)]
            if new_bal == self.next_balance:
                self.branches = [min(9, x+1) for x in self.branches]
        elif self.branches[level] == 0:
            self.branches[level] = 1
            new_bal[level] = balance
        elif new_bal[level] == balance and self.branches[level] < 9:
            self.branches[level] += 1
        else:
            new_bal[level] = balance
        self.set_vals((self.branches, new_bal))

    def on_mouse_rightclick(self, pos):
        level, balance = pos
        new_bal = self.next_balance[:]
        if level == -1:
            new_bal = [balance / 9 * (9-i) for i in range(10)]
            if new_bal == self.next_balance:
                self.branches = [max(0, x-1) for x in self.branches]

        else:
            if self.branches[level] == 0:
                return
            else:
                self.branches[level] = max(0, self.branches[level]-1)
                if self.branches[level] == 0:
                    new_bal[level] = 0
        self.set_vals((self.branches, new_bal))

    def set_vals(self, vals):
        branches, balance = vals
        self.branches = branches[:]
        self.next_balance = balance[:]
        self.dv = [(self.next_balance[i] - self.balance[i]) / 1000 for i in range(10)]

    def move_vals(self, dt):
        for i in range(10):
            if self.balance[i] > self.next_balance[i]:
                self.balance[i] = max(self.next_balance[i], self.balance[i]+self.dv[i]*dt)
            elif self.balance[i] < self.next_balance[i]:
                self.balance[i] = min(self.next_balance[i], self.balance[i]+self.dv[i]*dt)

    def to_json(self):
        return (self.branches, self.next_balance)

    def __call__(self, x):
        i = 9-int(x*9.99)
        return self.branches[i], (self.balance[i]+1) / (10)


class RadioButton(spg.EventReader):

    def __init__(self, screen_size, y, init=0, text='Branch Type', buttons=('-|', '-|-', 'T')):
        self.screen_size = screen_size
        self.y = y
        self.val = init
        self.text = text
        tree_base.PARAMETERS[text] = self
        self.buttons = buttons

    def mouse_map(self, x, y):
        sw, sh = self.screen_size
        if spg.in_box(x, y, (sw-240, self.y-20, 200, 50)):
            return int((x - sw+240) / 67)

    def draw(self, screen):
        sw, sh = self.screen_size
        with screen.part(sw-245, self.y, 210, 41, (130, 160, 130)) as part:
            if self._mouse_pos is not None:
                i = self._mouse_pos
                part.rect([i*67, 0, 67, 40], (255, 200, 200, 200))

            for i, t in enumerate(self.buttons):
                part.write(i*67+33, 0, t, pos='.', col=0, bold=True)
                part.write(i*67+33, 20, 'X' if i==self.val else '.', pos='.', col=LIGHT)

        screen.write(sw-240, self.y-24, self.text, col=LIGHT)

    def on_mouse_click(self, v):
        self.val = v

    def set_vals(self, v):
        self.val = v

    def to_json(self):
        return self.val

    def __call__(self):
        return self.val


class Button(spg.EventReader):

    def __init__(self, screen_size, y, text, action):
        self.screen_size = screen_size
        self.y = y
        self.text = text
        self.action = action

    def mouse_map(self, x, y):
        sw, sh = self.screen_size
        if spg.in_box(x, y, (sw-240, self.y, 200, 40)):
            return True

    def draw(self, screen):
        sw, sh = self.screen_size
        with screen.part(sw-240, self.y, 200, 41) as part:
            if self._mouse_pos is not None:
                part.fill((255, 200, 200, 200))
            else:
                part.fill((255, 200, 200, 50))

            part.write(100, 13, self.text, col=(0, 0, 0), pos='.')

    def on_mouse_click(self, v):
        self.action()


class Presets(spg.EventReader):

    def __init__(self, screen_size, y, action):
        self.screen_size = screen_size
        self.y = y
        self.action = action
        self.len = len(PRESETS)

    def mouse_map(self, x, y):
        sw, sh = self.screen_size
        if spg.in_box(x, y, (sw-240, self.y, 200, 30*self.len//2)):
            return int((y - self.y) / 30) * 2 + int((x - sw+240) / 100)

    def draw(self, screen):
        sw, sh = self.screen_size
        with screen.part(sw-240, self.y, 210, 30*self.len//2+1) as part:
            for i, (t, _) in enumerate(PRESETS):
                x, y = i%2, i//2
                if self._mouse_pos == i:
                    part.rect([x*100, y*30, 98, 28], (255, 200, 200, 200))
                else:
                    part.rect([x*100, y*30, 98, 28], (255, 200, 200, 50))
                part.write(x*100+50, y*30+10, t, pos='.', col=0, bold=False)

        screen.write(sw-240, self.y-24, 'Presets', col=LIGHT)

    def on_mouse_click(self, v):
        self.action(PRESETS[v][1])



class SegmentCounter(spg.EventReader):

    def __init__(self, screen_size, y, visual_space):
        self.screen_size = screen_size
        self.y = y
        self.visual_space = visual_space

    def draw(self, screen):
        sw, sh = self.screen_size
        screen.write(sw-240, self.y, f'Current segments on screen: \n{len(self.visual_space)}', col=LIGHT)


