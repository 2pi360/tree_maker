from math import pi, acos, cos, sin, isclose, copysign, log
import random


class TooManyBranches(Exception):
    pass


def procrustes(a, x, b):
    return max(a, min(x, b))


def sqrt(x):
    if isclose(x, 0, abs_tol=1e-6):
        return 0
    else:
        return x**0.5


class Vector3d():
    def __init__(self, x, y, z, dx, dy, dz, r=0):
        self.x = x
        self.y = y
        self.z = z
        self.dx = dx
        self.dy = dy
        self.dz = dz
        self.r = r
        self.internal_rot = r
        if self.len == 0:
            raise ValueError('Zero len vecror')

    @property
    def is_vertical(self):
        return self.dx == self.dz == 0

    @property
    def len(self):
        return (self.dx**2 + self.dy**2 + self.dz**2)**0.5

    @len.setter
    def len(self, val):
        if val == 0:
            raise ValueError('length cannot be zero')
        k = val / self.len
        self.dx *= k
        self.dy *= k
        self.dz *= k
        if self.len == 0:
            raise ValueError('Zero len vecror')

    @property
    def angl(self):
        """ angle to vertical """
        cos_angl = self.dy / self.len
        return acos(cos_angl)

    @angl.setter
    def angl(self, val):
        self.define(angl=val)

    @property
    def rot(self):
        """ angle to horizontal """
        if self.is_vertical:
            return self.internal_rot

        try:
            cos_rot = self.dx / (self.dx**2 + self.dz**2)**0.5
        except:
            raise ValueError(f'error determining rotation;\n'
                             f' dx={self.dx}\n'
                             f' dy={self.dy}\n'
                             f' dz={self.dz}\n')
        return copysign(acos(cos_rot), self.dz)

    @rot.setter
    def rot(self, val):
        self.define(rot=val)

    #--------

    def define(self, length=None, angl=None, rot=None):
        length = length if length is not None else self.len
        angl = angl if angl is not None else self.angl
        rot = rot if rot is not None else self.rot
        if angl == 0:
            y = length
            x = z = 0
        elif isclose(angl, pi/2, abs_tol=1e-5):
            y = 0
            x = cos(rot) * length
            z = sqrt(length**2 - x**2)
            z = copysign(z, sin(rot))
        else:
            y = cos(angl) * length
            x = cos(rot) * sqrt(length**2 - y**2)
            z = sqrt(length**2 - x**2 - y**2)
            z = copysign(z, sin(rot))

        if isinstance(x, complex) or isinstance(y, complex) or isinstance(z, complex):
            raise ValueError(f'some error were created;\n'
                             f' dx={self.dx} --> {x}\n'
                             f' dy={self.dy} --> {y}\n'
                             f' dz={self.dz} --> {z}\n'
                             f' angl={angl}\n'
                             f' rot={rot}')
        self.dx = x
        self.dy = y
        self.dz = z
        self.internal_rot = rot


    def rotateRodrigues(self, rot_axis, angle):
        if angle == 0:
            return
        x = cos(rot_axis)
        z = sin(rot_axis)
        a = sin(angle)
        b = 1 - cos(angle)

        dx = ( self.dx * (1 - b * z * z) +
               self.dy * a * z +
               self.dz * b * x * z)
        dy = (-self.dx * a * z +
               self.dy * (1 - b) +
               self.dz * a * x)
        dz = ( self.dx * b * x * z +
              -self.dy * a * x +
               self.dz * (1 - b * x * x))

        self.dx = dx
        self.dy = dy
        self.dz = dz

    #--------

    def copy(self):
        return Vector3d(self.x, self.y, self.z, self.dx, self.dy, self.dz, self.r)

    def grow(self):
        return Vector3d(self.x + self.dx, self.y + self.dy, self.z + self.dz, self.dx, self.dy, self.dz, self.r)

    def branch(self, angl, r=0, extra_rot=0):
        new = Vector3d(self.x, self.y, self.z, 0, self.len, 0, r=self.r+r)
        new.define(angl=angl, rot=self.r+extra_rot)
        new.rotateRodrigues(self.rot+pi/2, self.angl)
        return new

    def rotate(self, angle):
        c = cos(angle)
        s = sin(angle)
        return Vector3d(self.x*c - self.z*s, self.y, self.z*c + self.x*s,
                        self.dx*c - self.dz*s, self.dy, self.dz*c + self.dx*s,
                        self.r)

    def to_coords(self):
        return self.z + self.dz/2, (self.x, self.y), (self.x+self.dx, self.y+self.dy)

    #--------

    def __str__(self):
        return f'{self.x:.1f}:{self.y:.1f}:{self.z:.1f} + [{self.dx:.1f}:{self.dy:.1f}:{self.dz:.1f}]'

    def __repr__(self):
        return f'Vector3d({self.x}, {self.y}, {self.z}, {self.dx}, {self.dy}, {self.dz}, {self.r})'


def return_1(): return 1

def return_2(x): return 0.3

def return_3(x): return 1

def return_br(x): return [1, 0.1]


VISUAL_SPACE = []

PARAMETERS = {
    'Resolution': return_1,
    'Max Segments on Screen': return_1,

    'Length': return_2,
    'Width': return_2,
    'Raise': return_2,

    'Branching': return_br,
    'Rotation': return_1,
    'Rotation After Branching': return_1,
    'Branch Angle': return_2,
    'Branching Type': return_1,

    'Chaos': return_2,
    'Branch Chaos': return_1,
    'Random Distribution': return_1,

    'Red': return_2,
    'Green': return_2,
    'Blue': return_2,
    }


def draw_line(vector, col, w):
    VISUAL_SPACE.append([vector, col, w])


def noise(scale):
    x = random.random()*2 - 1
    if scale <=0:
        return 0
    t = PARAMETERS['Random Distribution']()
    if t == 0:
        return x * scale
    elif t == 1:
        return copysign(log(abs(x)*2), x) * scale
    elif t == 2:
        #return copysign(log(abs(x)*2)**2, x) * scale / 2.82
        return random.gauss(0, scale)


def next_seed(old_seed=None):
    if old_seed is not None:
        random.seed(old_seed)
    return random.randbytes(8)



if __name__ == '__main__':
    # tests
    V = Vector3d(0, 0, 0, 0, 10, 0, 0)
    print(V)
    C = V.copy()
    C.define(rot=36.128315516282626, angl=pi/2)
    print(C)
   # C.rotateRodrigues(pi/2, pi/2)
    print(C.rot)

