import sys
from statistics import mean

import pygame
import simplified_pygame as spg


SCREEN_SIZE = [640, 420]


SCREEN = spg.PyGameWindow(
    w=SCREEN_SIZE[0],
    h=SCREEN_SIZE[1],
    caption='Tree Maker',
    use_icon=False,
    bg_color=(130, 160, 130),
    default_font='cambria',
    resizable=True)


class AppControlls(spg.EventReaderAsClass):

    def on_key_escape():
        SCREEN.exit()


import buttons


CONTROLS = [buttons.SingleController(SCREEN_SIZE, 60, 0.5, 'rotation'),
            buttons.BranchingController(SCREEN_SIZE, 100, [1]*10, [0]*10),
            #buttons.Presets(SCREEN_SIZE, 100, None),
            #buttons.RadioButton(SCREEN_SIZE, 350)
            buttons.Button(SCREEN_SIZE, 350, 'BUTTON', None)
            ]


for events, time_passed, pressed_keys in SCREEN.main_loop(framerate=60):
    AppControlls.read_events(events, time_passed, pressed_keys)
    for C in CONTROLS:
        C.read_events(events, time_passed, pressed_keys)
        if hasattr(C, 'move_vals'):
            C.move_vals(time_passed)
        C.draw(SCREEN)
        SCREEN.write(10, 10, str(round(CONTROLS[0](), 2)))
        SCREEN.write(10, 50, str(CONTROLS[1](0)))
        SCREEN.write(10, 100, str(CONTROLS[1](0.5)))
        SCREEN.write(10, 150, str(CONTROLS[1](0.99)))

