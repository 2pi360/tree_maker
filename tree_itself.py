from tree_base import draw_line, Vector3d, VISUAL_SPACE
from tree_base import noise, next_seed, procrustes
from tree_base import PARAMETERS, TooManyBranches
from math import pi
import random



def make_tree():
    res = 1 / int(10**(PARAMETERS['Resolution']()+1))
    branching_type = PARAMETERS['Branching Type']()
    max_segments = {0: 1000, 1: 3000, 2: 10000}[int(PARAMETERS['Max Segments on Screen']())]

    # set up branching points
    branching_points = [PARAMETERS['Branching'](w/10+0.05) for w in range(10)]
    if branching_type == 0:
        branching_points = [[(i + (j+1)/(br+1)) / 10, 1, bb] for i, (br, bb) in enumerate(branching_points) for j in range(br)]
    else:
        branching_points = [(i/10 + 0.05, br, bb) for i, (br, bb) in enumerate(branching_points) if br]

    old_seed = next_seed()

    branches = [(Vector3d(0, 0, 0, 0, 10, 0, 0), 1, branching_points)]

    while branches:
        vector, width, branching_points = branches[0]
        branches = branches[1:]
        old_seed = next_seed(old_seed)

        while width >= 0.01:
            if len(VISUAL_SPACE) >= max_segments:
                raise TooManyBranches

            if branching_points and branching_points[-1][0] >= width:
                # modify branch
                vector.r += pi * PARAMETERS['Rotation']()
                br_w, n, bb = branching_points[-1]
                branching_points = branching_points[:-1]

                # add new branches
                new_angle = pi * (1-PARAMETERS['Branch Angle'](width))
                #new_width = width * (bb if branching_type < 2 else 1)
                new_width = min(width, bb) if branching_type < 2 else 1
                new_r = pi*PARAMETERS['Rotation After Branching']()
                new_branching = [(w, br, bb) for w, br, bb in branching_points if w < new_width and w < br_w-0.09]
                eps = PARAMETERS['Branch Chaos']() / 3

                for i in range(n):
                    extra_rot = 2*pi*i/n
                    branches.append((vector.branch(angl=new_angle, r=new_r, extra_rot=extra_rot), procrustes(0, new_width+noise(eps), width), new_branching))

                # continue branch
                if branching_type == 2:
                    width = 0

            else:
                # if branching, cut
                if branching_points and branching_points[-1][0] > width - res + 0.01:
                    seg = width - branching_points[-1][0]
                   # print(branching_points[-1][0], width - res, seg)
                else:
                    seg = res

                # modify branch
                eps = PARAMETERS['Chaos'](width)
                new_len = 1000 * seg * (PARAMETERS['Length'](width) + 0.01)
                new_rot = (vector.rot + noise(eps)) % (2 * pi)
                new_angl = max(vector.angl - 20 * seg * (PARAMETERS['Raise'](width)-0.5), 0) + noise(eps)
                vector.define(length=new_len, angl=new_angl, rot=new_rot)

                # draw branch
                col = PARAMETERS['Red'](width)*250, PARAMETERS['Green'](width)*250, PARAMETERS['Blue'](width)*250
                draw_line(vector, col, PARAMETERS['Width'](width)*20)

                # continue branch
                vector = vector.grow()
                width -= seg


if __name__ == '__main__':
    make_tree()
    print(len(VISUAL_SPACE))
