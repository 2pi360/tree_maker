from math import pi
import importlib
import random
import tkinter
from tkinter import messagebox
from tkinter import filedialog
import json


import simplified_pygame as spg

import tree_itself
from tree_base import PARAMETERS, VISUAL_SPACE, next_seed, procrustes

import buttons


SCREEN_SIZE = []
ORIGIN = []
SEED_0 = random.getstate()
ACTIVE_CONTROLS = 2


def switch_panel(new):
    global ACTIVE_CONTROLS
    ACTIVE_CONTROLS = new
    window_resize(*SCREEN_SIZE)


def window_resize(w, h):
    SCREEN_SIZE[:] = [w, h]
    ORIGIN[:] = [(SCREEN_SIZE[0]-(250 if ACTIVE_CONTROLS > 0 else 0)) / 2, SCREEN_SIZE[1] - 100]


def copy_to_clipboard():
    # create tree descroption
    text = json.dumps({C.text: C.to_json() for page in CONTROLS[2:] for C in page if hasattr(C, 'to_json')})
    # insert newlines
    text = text.replace(', "', ',\n"')
    text = '{\n' + text[1:]

    # copy description to clipboard
    # this function copied from stack overflow
    root = tkinter.Tk()
    root.withdraw()
    root.clipboard_clear()
    root.clipboard_append(text)
    root.update() # now it stays on the clipboard after the window is closed
    root.destroy()


def load_from_clipboard():
    # create tree descroption
    text = json.dumps({C.text: C.to_json() for page in CONTROLS[2:] for C in page})
    root = tkinter.Tk()
    text = root.clipboard_get()
    try:
        set_from_json(text)
    except Exception as error:
        r.wm_withdraw()
        messagebox.showerror('Error', str(error))
    root.destroy()


def set_from_json(text):
    settings = json.loads(text)
    if "Branch Balance" in settings:
        if isinstance(settings['Branching'], (int, float)):
            settings['Branching'] = [settings['Branching']]*10
        bb = settings['Branch Balance']
        if isinstance(bb, (int, float)):
            bb = [bb]*10
        bb = [bb[i]*(10-i)-1 for i in range(10)]
        settings['Branching'] = (settings['Branching'], bb)
        del settings['Branch Balance']

    for k, vals in settings.items():
        PARAMETERS[k].set_vals(vals)


def save_image():
    root = tkinter.Tk()
    root.withdraw()
    filename = filedialog.asksaveasfilename(defaultextension='.png', initialfile='tree.png')
    if filename:
        if ACTIVE_CONTROLS == 0:
            LAYER.save(filename)
        else:
            LAYER.crop(0, 0, SCREEN_SIZE[0]-250, SCREEN_SIZE[1]).save(filename)
    root.destroy()


def reseed():
    global SEED_0
    SEED_0 = next_seed(SEED_0)


class AppControlls(spg.EventReaderAsClass):
    def on_key_escape():
        SCREEN.exit()
    def on_window_resize(w, h):
        window_resize(w, h)
        SCREEN.set_game_resolution(w, h)
    def on_key_0(): switch_panel(0)
    def on_key_1(): switch_panel(1)
    def on_key_2(): switch_panel(2)
    def on_key_3(): switch_panel(3)
    def on_key_4(): switch_panel(4)
    def on_key_5(): switch_panel(5)
    def on_key_6(): switch_panel(6)
    def on_key_7(): switch_panel(7)

    def on_key_f4(): save_image()
    def on_key_f5(): copy_to_clipboard()
    def on_key_f6(): load_from_clipboard()
    def on_key_f9(): reseed()


class Folders(spg.EventReaderAsClass):

    def mouse_map(x, y):
        sw, sh = SCREEN_SIZE
        if spg.in_box(x, y, (sw-30, 0, 30, 8*75)):
            return int(y / 75)

    @classmethod
    def draw(cls, screen):
        sw, sh = SCREEN_SIZE
        with screen.part(sw-30, 0, 30, 8*75) as part:
            if cls._mouse_pos is not None:
                i = cls._mouse_pos
                part.rect([0, i*75, 40, 75], (255, 255, 255, 200))

            for i, t in enumerate(FOLDERS):
                part.write_vert(10, i*75+37, t, pos='.', col=(0, 0, 0))

    def on_mouse_click(i):
        switch_panel(i)


def draw_line(x0, y0, x1, y1, col, w):
    r, g, b, a = col
    LAYER.line((x0+ORIGIN[0], y0+ORIGIN[1], x1-x0, y1-y0), col, max(1, int(w)))
    if w>5:
        LAYER.circle((x0+ORIGIN[0], y0+ORIGIN[1]), w/2-1, col)
    if w>3 and (abs(x1-x0) < abs(y0-y1)):
        LAYER.line((x0+w/2+ORIGIN[0]-1/2, y0+ORIGIN[1],  x1-x0-1/2, y1-y0), (0, 0, 0, a), 1)


def init_tree():
    random.seed(SEED_0)
    VISUAL_SPACE[:] = []
    try:
        tree_itself.make_tree()
    except tree_itself.TooManyBranches:
        pass


window_resize(1250, 800)
SCREEN = spg.PyGameWindow(
    w=SCREEN_SIZE[0],
    h=SCREEN_SIZE[1],
    bg_color=(130, 160, 130),
    caption='Tree Maker',
    use_icon=False,
    default_font='cambria',
    resizable=True)


FOLDERS = ['View', 'Settings', 'Presets', 'General', 'Angles', 'Branches', 'Chaos', 'Color']

CONTROLS = [
    [],
    [
        buttons.SingleController(SCREEN_SIZE, 30, 0.5, 'Zoom'),
        buttons.SingleController(SCREEN_SIZE, 80, 0.5, 'Display Rotation Speed'),
        buttons.SingleController(SCREEN_SIZE, 130, 0.5, 'Fog'),
        buttons.SingleController(SCREEN_SIZE, 180, 0.5, 'Sliders Speed'),
        buttons.RadioButton(SCREEN_SIZE, 230, 1, 'Max Segments on Screen', buttons=['1 000', '3 000', '10 000']),
        buttons.SegmentCounter(SCREEN_SIZE, 300, VISUAL_SPACE),

        buttons.SingleController(SCREEN_SIZE, 400, 0.5, 'Resolution'),

        buttons.Button(SCREEN_SIZE, 600, '[F5] Copy to Clipboard', action=copy_to_clipboard),
        buttons.Button(SCREEN_SIZE, 650, '[F6] Load from Clipboard', action=load_from_clipboard),
        buttons.Button(SCREEN_SIZE, 700, '[F4] Save image', action=save_image),
    ],
    [
        buttons.Presets(SCREEN_SIZE, 30, action=set_from_json),
    ],
    [
        buttons.MultipleController(SCREEN_SIZE, 30, 0.5, 'Length'),
        buttons.MultipleController(SCREEN_SIZE, 300, [i/9 for i in range(10)], 'Width'),
    ],
    [
        buttons.MultipleController(SCREEN_SIZE, 30, 0.60, 'Branch Angle'),
        buttons.MultipleController(SCREEN_SIZE, 300, 0.55, 'Raise'),
    ],
    [
        buttons.BranchingController(SCREEN_SIZE, 30, [1]*10, [4, 4, 3, 3, 2, 2, 1, 1, 0, 0], 'Branching'),
        buttons.RadioButton(SCREEN_SIZE, 300, 0, 'Branching Type'),
        buttons.SingleController(SCREEN_SIZE, 400, 0.6, 'Rotation'),
        buttons.SingleController(SCREEN_SIZE, 470, 0.5, 'Rotation After Branching'),
    ],
    [
        buttons.MultipleController(SCREEN_SIZE, 30, 0.0, 'Chaos'),
        buttons.RadioButton(SCREEN_SIZE, 300, 0.0, 'Random Distribution', buttons=['Uniform', 'Exp', 'Gauss']),
        buttons.SingleController(SCREEN_SIZE, 370, 0.0, 'Branch Chaos'),
        buttons.Button(SCREEN_SIZE, 420, '[F9] Resample', action=reseed),
    ],
    [
        buttons.MultipleController(SCREEN_SIZE, 30, [0.5-i/20 for i in range(10)], 'Red'),
        buttons.MultipleController(SCREEN_SIZE, 300, [0.4-i/30 for i in range(10)], 'Green'),
        buttons.MultipleController(SCREEN_SIZE, 570, 0, 'Blue')
    ]]


MOVABLE = [C for panel in CONTROLS for C in panel if hasattr(C, 'move_vals')]


angle = 0

for events, time_passed, pressed_keys in SCREEN.main_loop(framerate=60):
    speed = 0.5 - PARAMETERS['Display Rotation Speed']()
    angle = (angle + pi/1000*time_passed * speed) % (pi*2)

    #SCREEN.sprite(0, 0, background)

    # interaction and changes
    AppControlls.read_events(events, time_passed, pressed_keys)
    Folders.read_events(events, time_passed, pressed_keys)
    Folders.draw(SCREEN)

    for C in CONTROLS[ACTIVE_CONTROLS]:
        C.read_events(events, time_passed, pressed_keys)
        C.draw(SCREEN)

    speed = 2 * PARAMETERS['Sliders Speed']() + 0.1
    for C in MOVABLE:
        C.move_vals(time_passed*speed)

    # make tree
    init_tree()

    # draw tree
    projections = [(vector.rotate(angle).to_coords(), col, w) for vector, col, w in VISUAL_SPACE]
    projections = sorted(projections)
    zoom = PARAMETERS['Zoom']() * 2
    fog = PARAMETERS['Fog']() * 2
    with SCREEN.layer((130, 160, 130, 0)) as LAYER:
        for (z, (x0, y0), (x1, y1)), col, w in projections:
            r, g, b = col
            a = min((200+z*fog), 255)
            if a <= 0:
                continue
            draw_line(
                x0*zoom, -y0*zoom, x1*zoom, -y1*zoom,
                (r, g, b, a),
                w)

